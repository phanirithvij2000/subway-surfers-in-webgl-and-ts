const title = `Subway Surf | WebGL`;
const desc = "The game should ideally be up here 👆";
const pattern = /([A-Z])\w+/;

module.exports = {
    title,
    desc,
    pattern,
};
