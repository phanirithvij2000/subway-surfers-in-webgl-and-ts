import { GL_BL_ASSETS } from "./init";
import { loadObj, loadImg, initBuffers, drawObject3D } from "./utils";
import { World } from "./world";
import { Coin } from "./coin";
import { mat4 } from "gl-matrix";


// declare global{
// interface Vector3 {
//     x: number,
//     y: number,
//     z: number
// }
// }
export class Train{
    position        : Vector3;
    rotation        : Vector3;
    dead            : boolean = false;
    speed           : Vector3;
    scateboardcount : number;
    bbox            : Bbox;
    texture         : HTMLImageElement;
    object          : any;
    gl              : WebGLRenderingContext;
    world           : World;
    buffers         : Buffer;
    coins           : Coin[];
    blocks          : {
        front   : Train;
        blocks  : any[];
    };
    lane            : number = 0;
    constructor(position: Vector3, world: World){
        this.position = {
            x: position.x,
            y: position.y + 11.761,
            z: position.z,
        };
        this.speed = {
            x : -0.5,
            y : 0,
            z : 0
        };
        this.rotation = {
            x : 0,
            y : 0,
            z : 0
        };

        this.lane = this.position.z / 20;

        this.bbox = {
            height: 26.45 / window['sclae'],
            length: 76.7 / window['sclae'],
            width : 16.7  / window['sclae']
        };
        this.world = world;
        this.gl = world.gl;
        this.texture = GL_BL_ASSETS.textures['trainblock'];
        this.object = GL_BL_ASSETS.objects['trainblock'];
        this.buffers = initBuffers(this.gl, this.object);
        this.coins = [];

        for (var i:number =0 ; i< 8 ;i ++){
            this.coins.push(
            new Coin(
                {
                    x: this.position.x + (i - 4) * 6,
                    y: this.position.y + 13,
                    z: this.position.z,
                },
                world.player, this)
            );
        }
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
        // draw this.object;
        this.coins.forEach(coin=>{
            coin.draw(projectionMatrix, programinfo, deltatime);
        });
    }

    tick(): void {
        if (this.position.x - this.world.player.position.x < -100){
            this.dead = true;
            return;
        }
        this.position.x += this.speed.x;
        this.position.y += this.speed.y;
        this.position.z += this.speed.z;

        this.coins.forEach(coin=>{
            if (!coin.dead) coin.tick();
        });
    };

    delete(trainlist: Train[]):void{
        let index : number = trainlist.indexOf(this);
        if (index > -1) {
            trainlist.splice(index, 1);
        }
    }

    deleteCoin(coin: Coin):void{ 
        let index : number = this.coins.indexOf(coin);
        if (index > -1) {
            this.coins.splice(index, 1);
        }
    }
}
