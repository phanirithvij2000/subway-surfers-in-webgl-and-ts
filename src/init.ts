import { World } from "./world";


/* import JAKE_OBJ from "./assets/objs/trex.obj";
import JAKE_IMG from "./assets/images/trex_2048x2048_1.png";

import JAKE_OBJ from "./assets/objs/birdv3.obj";
import JAKE_IMG from "./assets/images/bird.png";

*/
import JAKE_OBJ from "./assets/objs/Muhammerv2.obj";
import JAKE_IMG from "./assets/images/Muhammer.png";

import train_slant from "./assets/objs/trainslant.obj";
import textu_slant from "./assets/images/trainslant.png";

import train_block_obj from "./assets/objs/trainblock.obj";
import train_block_img from "./assets/images/trainblock.png";
// import train_block_obj from "./assets/objs/pole.obj";
// import train_block_img from "./assets/images/pole.png";

import TRACK_obj from "./assets/objs/tracks.obj";
import TRACK_img from "./assets/images/tracks.jpg";

import Pole_obj from "./assets/objs/pole.obj";
import Pole_img from "./assets/images/pole.png";

import Wall_obj from "./assets/objs/wall.obj";
import Wall_img from "./assets/images/wall.jpg";

import Magent_Image from "./assets/images/magnet.png";
import Magent_OBJ from "./assets/objs/magnet.obj";

import Jumper_OBJ from "./assets/objs/shoes.obj";
import Jumper_Image from "./assets/images/shoe.png";

import ScateBoard_OBJ from "./assets/objs/scateboard.obj";
import ScateBoard_Image from "./assets/images/scateboard.png";

import BIRD_Image from "./assets/images/bird.png";
import BIRD_OBJ from "./assets/objs/bird.obj";

import COIN_OBJ from "./assets/objs/coin.obj";
import COIN_IMAGE from "./assets/images/coin.png";

import GRND_OBJ from "./assets/objs/ground.obj";
import GRND_IMAGE from "./assets/images/sand.jpg";

import DOG_OBJ from "./assets/objs/dogv2.obj";
import DOG_IMAGE from "./assets/images/dog_body.jpg";
// import DOG_IMAGE from "./assets/images/trex_2048x2048_1.png";

import Police_OBJ from "./assets/objs/policev2.obj";
import Police_IMAGE from "./assets/images/trex_2048x2048_1.png";

import TunnelMiddle_OBJ from "./assets/objs/tunnelmiddle.obj";
import TunnelMiddle_IMAGE from "./assets/images/tunnelmiddle.png";

import Croucher_OBJ from "./assets/objs/croucher.obj";
import Croucher_IMAGE from "./assets/images/croucher.png";

import Jumponce_OBJ from "./assets/objs/jumponce.obj";
import Jumponce_IMAGE from "./assets/images/jumponce.png";

import Jetpack_OBJ from "./assets/objs/jetpack.obj";
import Jetpack_IMAGE from "./assets/images/jetpack.png";

import { loadObj, loadImg } from "./utils";

export var GL_BL_ASSETS = {
    objects : {},
    textures : {},
    images : {}
};

window['GL_BL_ASSETS'] = GL_BL_ASSETS;

export var initWorld = (gl : WebGLRenderingContext, program: Program, cb: Function) : void=>{
    console.log('Init world');
    loadallObjs(gl, (obj: any)=>{
        console.log("DONE WITH TIS SIT", obj);
        var w = new World(gl, program, (wo: World)=>{
            console.log("in constructor callback", wo);
            console.log('Init world after', wo.player.texture);
            cb(wo);
        });

        console.log('MOGO');
    })
};

export var loadallObjs = (gl: WebGLRenderingContext, cb:Function)=>{
    /* Load all objects at the beginning itself to avoid undefined callback hell */
    var OBJS_IMAGES : any[] = [
        {
            obj :Croucher_OBJ,
            name:'croucher',
            img :Croucher_IMAGE
        },
        {
            obj :Jetpack_OBJ,
            name:'jetpack',
            img :Jetpack_IMAGE
        },
        {
            obj :Wall_obj,
            name:'wall',
            img :Wall_img
        },
        {
            obj :Jumponce_OBJ,
            name:'jumponce',
            img :Jumponce_IMAGE
        },
        {
            obj :Pole_obj,
            name:'pole',
            img :Pole_img
        },
        {
            obj :JAKE_OBJ,
            name:'jake',
            img :JAKE_IMG
        },
        {
            obj :TunnelMiddle_OBJ,
            name:'tunnelmiddle',
            img :TunnelMiddle_IMAGE
        },
        {
            obj :train_block_obj,
            name:'trainblock',
            img :train_block_img
        },
        {
            obj :TRACK_obj,
            name:'track',
            img :TRACK_img
        },
        {
            obj :Jumper_OBJ,
            name:'jumper',
            img :Jumper_Image
        },
        {
            obj :Magent_OBJ,
            name:'magnet',
            img :Magent_Image
        },
        {
            obj :ScateBoard_OBJ,
            name:'scateboard',
            img :ScateBoard_Image
        },
        {
            obj :BIRD_OBJ,
            name:'bird',
            img :BIRD_Image
        },
        {
            obj :COIN_OBJ,
            name:'coin',
            img :COIN_IMAGE
        },
        {
            obj :Police_OBJ,
            name:'police',
            img :Police_IMAGE
        },
        {
            obj :DOG_OBJ,
            name:'dog',
            img :DOG_IMAGE
        },
        {
            obj :GRND_OBJ,
            name:'ground',
            img :GRND_IMAGE
        },
    ];

    var counter = 0;

    OBJS_IMAGES.forEach((obj)=>{
        loadObj(obj.obj,(doneObj: any)=>{
            GL_BL_ASSETS.objects[obj.name] = doneObj;
            console.log("Done loading OBJ", obj.obj);
            loadImg(obj.img, gl,(imgdone : HTMLImageElement, texture: WebGLTexture)=>{
                GL_BL_ASSETS.textures[obj.name] = texture;
                GL_BL_ASSETS.images[obj.name] = imgdone;
                console.log("Done loading IMAGE ", obj.img, imgdone);
                counter++;
                if (counter == OBJS_IMAGES.length){
                    cb(obj);
                }
            });
        });
    });
};