import { Train } from "./train";
import { Jake } from "./jake";
import { GL_BL_ASSETS } from "./init";
import { vec3, mat4 } from "gl-matrix";
import { initBuffers, drawObject3D } from "./utils";

export class Jetpack{
    position   : Vector3;
    rotation   : Vector3;
    candraw    : boolean = false;
    object     : any;
    dead       : boolean = false;
    lane       : number = 0;
    buffers    : Buffer;
    gl         : WebGLRenderingContext;
    texture    : HTMLImageElement;
    player     : Jake;
    alljetpacks: Jetpack[];
    constructor(position: Vector3, player: Jake){
        this.player = player;
        this.position = position;
        this.gl = player.gl;
        this.rotation = {
            x: 0, y: 0, z:0,
        };
        this.lane = this.position.z / 20;
        this.object = GL_BL_ASSETS.objects['jetpack'];
        this.texture = GL_BL_ASSETS.textures['jetpack'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    activate(){
        this.candraw = true;
    }

    deactivate(){
        this.candraw = false;
        this.dead = true;
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        // if (this.candraw){
        this.candraw && drawObject3D(this.gl, programinfo, projectionMatrix, this);
        // }
        //draw from this.object and this.texture after binding it at a texture_slot?
    };

    tick(position?: vec3):void{
        if (this.position.x + 100 < this.player.position.x){
            this.dead = true;
        }
    }
}
