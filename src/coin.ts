import { Train } from "./train";
import { Jake } from "./jake";
import { GL_BL_ASSETS } from "./init";
import { initBuffers, drawObject3D } from "./utils";
import { mat4 } from "gl-matrix";
import { Track } from "./tracks";

export class Coin{
    position   : Vector3;
    initpostion: Vector3;
    rotation   : Vector3;
    parent     : Train|Track = null;
    candraw    : boolean;
    object     : any;
    gl         : WebGLRenderingContext;
    texture    : HTMLImageElement;
    buffers    : Buffer;
    player     : Jake;
    textureSlot: number;
    program    : Program;
    bbox       : Bbox;
    dead       : boolean = false;
    constructor(position: Vector3, player: Jake, parent?:Train|Track){
        this.player = player;
        if (parent){
            this.parent = parent;
        }
        this.gl     = player.gl;
        this.program = player.program;
        this.position = position;
        this.initpostion = {
            x: position.x,
            y: position.y,
            z: position.z,
        };

        this.bbox = {
            height : 4.48,
            length : 1 /* * Math.cos(rotation.y) + 4.48 */ /* *Math.sin(rotation.y) */,
            width  : /* 1 * Math.sin(rotation.y) + */ 4.48 /* *Math.cos(rotation.y) */,
        };

        this.rotation = {
            x: 0,
            y: 0,
            z: 90
        };

        this.object = GL_BL_ASSETS.objects['coin'];
        this.texture = GL_BL_ASSETS.textures['coin'];
        this.buffers = initBuffers(player.gl, this.object);
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        if (!this.dead) {
            drawObject3D(this.gl, programinfo, projectionMatrix, this);
        };
        //draw from this.object and this.texture after binding it at a texture_slot?
    };

    tick(position?: Vector3):void{
        if(this.parent){
            this.position = {
                x: this.position.x + this.parent.speed.x,
                y: this.position.y,
                z: this.position.z
            };
        }
        this.rotation.y += 3;
/* 
        this.bbox.length = 1 * Math.cos(this.rotation.y)  + 4.48 * Math.sin(this.rotation.y) ;
        this.bbox.width  = 1 * Math.sin(this.rotation.y)  + 4.48 * Math.cos(this.rotation.y) ; */
    }

}