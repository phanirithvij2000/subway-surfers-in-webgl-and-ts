import {Jake} from "./jake";
import { GL_BL_ASSETS } from "./init";
import { mat4 } from "gl-matrix";
import { drawObject3D, initBuffers } from "./utils";

export class ScateBoard{
    player : Jake;
    active : boolean;
    candraw: boolean;
    object : any;
    position: Vector3;
    rotation: Vector3;
    buffers : Buffer;
    texture: HTMLImageElement;
    gl     : WebGLRenderingContext;
    constructor(player: Jake){
        this.player = player;
        this.gl = player.gl;
        this.active = false;
        this.position = {
            x:player.position.x,
            y:player.position.y - player.bbox.height / 2,
            z:player.position.z,
        };
        this.rotation = {x:0,y:0,z:0};
        this.object = null;
        this.object = GL_BL_ASSETS.objects['scateboard'];
        this.texture = GL_BL_ASSETS.textures['scateboard'];
        this.buffers = initBuffers(this.gl,this.object);
    }

    tick() : void {
        this.position = {
            x:this.player.position.x,
            y:this.player.position.y - this.player.bbox.height / 2,
            z:this.player.position.z,
        };
    }

    activate():void{
        if (!this.active){
            console.log("Activating");
            this.candraw = true;
            this.active = true;
            // this.player.scateboardcount --;
        }
        else console.log("Already active");
        return;
    }

    deactivate():void{
        this.candraw = false;
        this.active = false;
    }

    draw(projectionMatrix : mat4, programinfo:Program, deltatime?: number) : void {
        // draw this.object;
        if (this.candraw && this.active){
            /* draw */
            // console.log("Drawing scateboard");
            drawObject3D(this.gl, programinfo, projectionMatrix, this);
        }
    }

}
