import { GL_BL_ASSETS } from "./init";
import { initBuffers, drawObject3D } from "./utils";

import { World } from "./world";
import { mat4 } from "gl-matrix";
import { Dog } from "./dog";

export class Police {
    position: Vector3;
    rotation: Vector3;
    speed: Vector3;
    texture: HTMLImageElement;
    object: any;
    world: World;
    gl: WebGLRenderingContext;
    buffers: Buffer;
    lane: number;
    program: Program;
    distance: number;
    dog : Dog;
    constructor(world: World, program?: Program) {
        if (program) {
            this.program = program;
        }
        this.world = world;
        this.position = { x: world.player.position.x, y: world.player.position.y, z: world.player.position.z };
        this.gl = world.gl;

        this.dog = new Dog(this);

        this.distance = 50;

        this.lane = world.player.lane;
        this.speed = {
            x: 0.5,
            y: 0.3,
            z: 0
        };
        this.rotation = {
            x: 0,
            y: 180,
            z: 0
        };

        this.object = GL_BL_ASSETS.objects['police'];
        this.texture = GL_BL_ASSETS.textures['police'];
        this.buffers = initBuffers(this.gl, this.object);
    }

    tick(data?: any): void | boolean {
        this.lane = this.world.player.lane;
        var player = this.world.player;
        if (player.dead){
            this.distance = 10;
        }
        this.position.x = player.position.x - (this.distance);
        this.position.z = player.position.z;
        if (this.position.y > 20){
            this.position.y = 0;
        }
        this.dog.tick();
    }
    draw(projectionMatrix: mat4, programinfo: Program, deltatime: number): void {
        // draw this.object;
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
        this.dog.draw(projectionMatrix, programinfo, deltatime);
    };
};
