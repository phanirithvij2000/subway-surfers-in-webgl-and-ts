import { ScateBoard } from "./scateboard";
import { Magnet } from "./magnet";
import { GL_BL_ASSETS } from "./init";
import { initBuffers, drawObject3D } from "./utils";

import { World } from "./world";

import { mat4 } from "gl-matrix";
import { Jumper } from "./jumper";
import { Coin } from "./coin";
import { Train } from "./train";
import { Jetpack } from "./jetpack";


declare global{
    interface Vector3 {
        x: number;
        y: number;
        z: number;
    }
    interface Bbox {
        width  : number;
        height : number;
        length : number;
        innerheight? : number;
        innerwidth? : number;
        innerlength? : number;
    }
};
export class Jake{
    bbox            : Bbox;
    position        : Vector3;
    rotation        : Vector3;
    score           : number;
    scatestarttime  : Date;
    jumpstarttime   : Date;
    magnetstarttime : Date;
    dimesions       : {
        bottom : number,
        top    : number,
        right  : number,
        left   : number,
    };
    dead            : boolean = false;
    jumping         : boolean = false;
    crouching       : boolean = false;
    headon          : boolean = false;
    flying          : boolean = false;
    isconfused      : boolean = false;
    ontrain         : boolean = false;
    onground        : boolean = true;
    success         : boolean = false;
    switchinglane    : boolean = false;
    didstunt         : boolean = false;
    movingleft       : boolean = false;
    movingright      : boolean = false;
    magnet          : Magnet;
    scateboard      : ScateBoard;
    shoes           : Jumper;
    shoesR           : Jumper;
    speed           : Vector3;
    scateboardcount : number;
    texture         : HTMLImageElement;
    object          : any;
    world           : World;
    gl              : WebGLRenderingContext;
    buffers         : Buffer;
    confusedcount   : number;
    lane            : number = 0;
    nextlane        : number = 0;
    program         : Program;
    jumpmax         : number;
    jumping_direction: number;
    switchstartpos   : number = 0;
    incross          : number = -0.3;
    incrossval       : number = -10;
    jetpack           : Jetpack;
    coincount        : number = 0;
    constructor(position: Vector3, world : World, program?: Program){
        this.jumping = false;
        if (program){
            this.program = program;
        }
        this.world = world;
        this.position = position;
        this.score = 0;
        this.confusedcount = 0;
        this.jumpmax = 1;
        this.dead = false;
        this.jumping = false;
        this.jumping_direction = 0;
        this.onground = true;
        this.headon = false;
        this.gl = world.gl;

        this.bbox = {
            width  : 4.113 / window['sclae'],
            height : 9.38 / window['sclae'],
            length : 3.96 / window['sclae'], 
        };
        
        this.scateboard = new ScateBoard(this);
        this.scateboard.active = false; /* active => scating */
        this.scatestarttime = null;

        this.magnet = new Magnet(this, position);
        this.magnetstarttime = null;

        this.jetpack = new Jetpack(position,this);
        this.jetpack.candraw = false;

        this.shoes = new Jumper(
            this, {
                x: position.x,
                y: position.y,
                z: position.z - this.bbox.width/2,
        });
        this.shoesR = new Jumper(
            this, {
                x: position.x,
                y: position.y,
                z: position.z + this.bbox.width/2,
        });

        this.jumpstarttime = null;

        this.speed = {
            x: 1.4,
            y: 1,
            z: 1,
        };
        this.rotation = {
            x: 0,
            y: 0,
            z: 0
        };

        this.scateboardcount = 10;
        this.object = GL_BL_ASSETS.objects['jake'];
        this.texture = GL_BL_ASSETS.textures['jake'];
        console.log(`Just before loading obj`, this.object);

        console.log("Don't mind me just trying to load buffers", this.buffers);
        this.buffers = initBuffers(this.gl,this.object);
        console.log("Don't mind me just done loading buffers", this.buffers);

    }

    tick(data?:any) : void {
        if (this.dead) {
            this.rotation.x = -90;
            this.rotation.y = -130;
            this.rotation.z = 20;
            return;
        }; /* if tick returns end game */

        if (this.flying){
            this.magnet.activate();
            this.jetpack.rotation.z = 90;
            this.jetpack.candraw = true;
            this.position.y += 2;
            if (this.position.y > 40){
                this.position.y = 40;
            }
        }

        this.score += 10;

        this.checkCollisions();

        if (this.isconfused){
            this.isconfused = false;
            this.world.police.distance /= 2;
            setTimeout(() => {
                clearInterval(window['jakeisshaking']); /* stop shaking */
            }, 2000);
            /* decrement after ten secs */
            setTimeout(()=>{
                this.world.police.distance *= 2;
                this.confusedcount --;
            }, 10000);
        }

        this.position.x += this.speed.x;
        var campos : Vector3 = this.world.cameraPos;
        this.world.cameraPos.x = (this.position.x - 0.2) - 70;
        this.world.cameraPos.y = /* this.position.y + */ 70;
        this.world.cameraPos.z = /* this.position.z + */ 0;
        // this.world.cameraPos = this.position;

        this.shoes.position = {
            x: this.position.x,
            y: this.position.y - 2 ,
            z: this.position.z - 1,
        };

        this.shoesR.position = {
            x: this.position.x,
            y: this.position.y - 2 ,
            z: this.position.z + 1,
        };

        // console.log(this.world.cameraPos == campos, "Cam");
        // console.log(this.world.cameraPos == window['world'].cameraPos, "Camw");
        this.crouch();
        this.jump();
        this.switchlane();

        this.scateboard.tick();
    }

    switchlane(){
        if (this.switchinglane && this.nextlane == 1){
            this.position.z += this.speed.z;
            if (this.position.z - this.switchstartpos > 22){
                this.lane++;
                this.nextlane = 0;
            }
        }
        if (this.switchinglane && this.nextlane == -1){
            this.position.z -= this.speed.z;
            if (this.switchstartpos - this.position.z > 22){
                this.lane--;
                this.nextlane = 0;
            }
        }
        if (this.switchinglane && this.nextlane == 0){
            this.movingleft = false;
            this.movingright = false;
            this.switchinglane = false;
        }
    };

    checkCollisions() {
        /* Check collisions */
        /* trains, crouchers, tunnels, poles */

        this.world.nearestTrains().forEach(train=>{
            if (train != undefined){
                // console.log(train, `A train baby ${Math.random()}`);
                if (this.headoncollidingTrain(train)){
                    this.dead = true;
                }else{
                    this.checkcoins(train.coins);
                }
            }
        });

/*         console.log(this.world.tracks[0].position.x, this.position.x, 0);
        console.log(this.world.tracks[3].position.x, this.position.x, 3);
        console.log(this.world.tracks[6].position.x, this.position.x, 6);
        console.log(this.world.tracks[9].position.x, this.position.x, 9);
        console.log(this.world.tracks[12].position.x, this.position.x, 12);
 */
        this.checkcoins(this.world.tracks[9].coins);
        this.checkcoins(this.world.tracks[10].coins);
        this.checkcoins(this.world.tracks[11].coins);
        this.checkcoins(this.world.tracks[12].coins);
        this.checkcoins(this.world.tracks[13].coins);
        this.checkcoins(this.world.tracks[14].coins);

        /* if there's a confused collision */
        /* not head */
        if (this.isconfused){
            this.confusedcount ++;
            window['jakeisshaking'] =
            setInterval(()=>{
                if (this.incrossval < -7){
                    this.incross = 0.7;
                }
                if (this.incrossval > 7) {
                    this.incross = -0.7;
                }
                this.rotation.x += this.incross;
                this.incrossval += this.incross;
            }, 10);
        }

        var nearestshoes = [this.world.jumpers[0], this.world.jumpers[1]];
        if (
            Math.abs(nearestshoes[0].position.x - this.position.x) < 10 &&
            this.jumping == false &&
            this.lane == nearestshoes[0].lane
            ){
            console.log({ass:nearestshoes.map(s=>s.position.x),bass:this.position.x});
            nearestshoes[0].dead = true;
            this.activateShoes();

        }

        var nearestjetpacks : Jetpack[]= [
            this.world.jetpacks[0],
            this.world.jetpacks[1],
            this.world.jetpacks[2]
        ];

        var choice = Math.random() > 0.999;
        if (choice){
            var rotta = setInterval(()=>{
                this.rotation.y += 1;
                if (this.rotation.y > 360 * 3){
                    this.rotation.y = -180;
                    clearInterval(rotta);
                }
            },10);
        }

        nearestjetpacks.forEach(
            j=>{
                if( j &&
                    Math.abs(j.position.x - this.position.x) < 10 &&
                    this.jumping == false &&
                    this.flying == false &&
                    this.lane == j.lane) {
                    j.dead = true;
                    this.speed.x *= 2;
                    this.flying = true;
                    this.rotation.y = 180;
                    setTimeout(() => {
                        this.flying = false;
                        this.jetpack.candraw = false;
                        this.rotation.y = -180;
                        var task = setInterval(() => {
                            this.position.y -= 3;
                            if (this.position.y<3){
                                this.position.y = 3;
                                clearInterval(task);
                            }
                        }, 10);
                        this.speed.x /= 2;
                        this.magnet.deactivate();
                    }, 10000);
                }
            }
        )

        if (this.confusedcount == 2 || this.headon){
            this.dead = true;
        }
    }

    activateScateboard() : void {
        if (!this.scateboard.active){
            this.speed.x *= 2;
            this.scateboard = new ScateBoard(this);
            this.scateboard.activate();
            setTimeout(() => {
                this.deactivateScateboard();
            }, 20000); /* deactivate after 20 sec */
            this.scateboardcount--;
            this.scatestarttime = new Date();
        } else {
            //already scating
            console.log('already scating ...');
        }
    }

    deactivateScateboard() : void{
        this.speed.x *= 1/2;
        this.scateboard.deactivate();
    }

    activateShoes() : void {
        if (!this.shoes.active){
            this.jumpmax = 2;
            // this.speed.y *= 3;
            this.shoes.activate();
            this.shoesR.activate();
            setTimeout(() => {
                this.deactivateShoes();
            }, 15000); /* deactivate after 15 sec */ 
        } else {
            console.log(`Already active ...`);
        }
    }

    crouch() : void {
        if (this.crouching){
            this.rotation.z = 90;
        }
    }

    jump() : void {
        if (this.jumping && this.jumping_direction==0){
            console.log('jump started');
            this.jumping_direction = 1;
        }
        if (this.jumping && this.jumping_direction == 1){
            if (this.position.y > this.jumpmax * 20){ /* atleast 9/2 + 21 when shoes */
                this.jumping_direction = -1;
            }
        }
        if (this.jumping && this.jumping_direction == -1){
           /*  var trainheight = this.world.obstacles.trains[0].bbox.height;
            var nearesttrains = this.world.nearestTrains();
            nearesttrains.forEach(t=>{
                if (
                    (this.position.y < trainheight + this.bbox.height / 2) &&
                    2 * Math.abs(this.position.x - t.position.x) > (t.bbox.length + this.bbox.length) &&
                    2 * Math.abs(this.position.z - t.position.z) > (t.bbox.width + this.bbox.width)
                ){
                    this.ontrain = true;
                    // this.world.obstacles.trains.filter(tr=>tr.position.)
                    // this.jumping = false;
                    // this.jumping_direction = 0;
                } else {
                    this.ontrain = false;
                }
            });
            if(this.ontrain){
                this.position.y = (nearesttrains[0].bbox.height + 1);
            } else { */
                this.onground = true;
                if (this.position.y < this.bbox.height / 2){
                    this.jumping = false;
                    // this.position.y = 0;
                    this.jumping_direction = 0;
                    this.position.y = this.bbox.height / 2;
                }
                // this.position.y = 0;
            /* } */
        }
        this.position.y += this.jumping_direction * this.speed.y;
        if (this.jumping && this.scateboard.active){
            if (this.didstunt){
                this.scateboard.rotation.y += 10;
                if (this.shoes.active){
                    this.rotation.z -= 5;
                }else {
                    this.rotation.y -= 5;
                }
                this.magnet.activate();
            }
        } else {
            if (this.didstunt){
                this.scateboard.rotation.y = 10;
                if (this.shoes.active){
                    this.rotation.z = 0;
                }else {
                    this.rotation.y = 0;
                }
                this.didstunt = false;
            }
        }

    };

    deactivateShoes() : void {
        this.jumpmax = 1;
        // this.speed.y /= 3;
        this.shoes.deactivate();
        this.shoesR.deactivate();
    };

    checkcoins(coinslist : Coin[]):void {
        coinslist.forEach(c=>{
            if (this.collidingOther(c) && !c.dead){
                c.dead = true;
                this.coincount++;
                this.score += 1000;
            }
        });
    };

    collidingOther(other: Coin|Jumper) : boolean{
       return(
            2 * Math.abs(other.position.x - this.position.x) < (this.bbox.length + other.bbox.length) &&
            2 * Math.abs(other.position.y - this.position.y) < (this.bbox.height + other.bbox.height) &&
            2 * Math.abs(other.position.z - this.position.z) < (this.bbox.width  + other.bbox.width )
        );
    };

    collidingJumper(other: Jumper) : boolean{
        return(
            2 * Math.abs(other.position.x - this.position.x) < (this.bbox.length + other.bbox.length) &&
            2 * Math.abs(other.position.y - this.position.y) < (this.bbox.height + other.bbox.height)
         );
     };

    headoncollidingTrain(train:Train) : boolean {
        return(
            (this.lane == train.lane) /* samelane */ &&
            train.position.x > this.position.x && 
            train.position.x - this.position.x < (this.bbox.length + train.bbox.length) / 2 &&
            2 * Math.abs(train.position.z - this.position.z) < (this.bbox.width  + train.bbox.width) &&
            this.position.y < train.bbox.height + (this.bbox.height/2)
        );
    };

    draw(projectionMatrix : mat4, programinfo:Program, deltatime: number) : void {
        // draw this.object;
        drawObject3D(this.gl, programinfo, projectionMatrix, this);
        this.scateboard.draw(projectionMatrix, programinfo, deltatime);
        this.shoes.draw(projectionMatrix, programinfo, deltatime);
        this.shoesR.draw(projectionMatrix, programinfo, deltatime);
        this.jetpack.draw(projectionMatrix, programinfo, deltatime);
        this.magnet.activate();
    };
};
